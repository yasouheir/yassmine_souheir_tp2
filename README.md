# yassmine_souheir_tp2
## Utilisation de IMHex et du script Python pour l'extraction des données GPS
# Utilisation de IMHex
Le logiciel IMHex est utilisé pour visualiser les fichiers binaires contenant les données GPS. Pour ce faire, ouvrez le fichier binaire dans IMHex et accédez à l'offset 0x248 où les trames de données commencent. Une fois que vous avez identifié la structure des trames de données à l'aide de l'éditeur de motifs, vous pourrez procéder à l'extraction des données nécessaires pour la conversion ultérieure.

## Utilisation du script Python pour l'extraction des données
Le script extract.py fourni permet d'extraire les données de localisation (latitude, longitude) à partir des trames de données du fichier binaire. Voici comment utiliser le script :

Assurez-vous d'avoir Python installé sur votre système.
Exécutez le script en fournissant le nom du fichier binaire à traiter.

Une fois le script exécuté, les données extraites seront enregistrées dans un fichier CSV nommé donnees_gps.csv.
Le script utilise le module struct pour interpréter les trames binaires du fichier et extrait les coordonnées de latitude et de longitude, les convertissant en valeurs exploitables pour la visualisation ultérieure.


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
yassmine souheir

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
